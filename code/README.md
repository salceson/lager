Requirements
------------
* Linux
* Erlang OTP 16.

How to run?
-----------

Simply after downloading type:
```
make
```

It will cause the project to compile.

After that simply run:
```
./start.sh
```
and it will launch the dummy_app.

Dummy_app is a simple "bank" server. It has the following commands:
```
dummy_app_server:deposit/1
dummy_app_server:withdraw/1
dummy_app_server:check_balance/0
```
