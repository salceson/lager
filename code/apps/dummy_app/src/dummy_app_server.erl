-module(dummy_app_server).

-behaviour(gen_server).

%% API
-export([start_link/0, deposit/1, withdraw/1, check_balance/0]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
     terminate/2, code_change/3]).

-record(state, {balance = 0}).

%% API
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

deposit(N) when is_integer(N) and (N > 0) ->
	gen_server:call(?MODULE, {deposit, N}).

withdraw(N) when is_integer(N) and (N > 0) ->
	gen_server:call(?MODULE, {withdraw, N}).

check_balance() ->
	gen_server:call(?MODULE, check_balance).

%% gen_server callbacks
init([]) ->
    lager:info("Bank account server ready."),
    {ok, #state{}}.

handle_call(check_balance, _From, State=#state{ balance = Balance }) ->
    lager:info("Checking balance: ~w", [lager:pr(State, ?MODULE)]),
    {reply, Balance, State};

handle_call({deposit, N}, _From, State=#state{ balance = Balance }) ->
    NewBalance = Balance + N,
    NewState = State#state{balance = NewBalance},
    Reply = {ok, NewBalance},
    lager:info("Deposited ~w, new balance: ~w", [N, NewBalance]),
    {reply, Reply, NewState};

handle_call({withdraw, N}, _From, State=#state{ balance = Balance }) when (N =< Balance) ->
    NewBalance = State#state.balance - N,
    NewState = State#state{balance = NewBalance},
    Reply = {ok, NewBalance},
    lager:info("Withdrawn ~w, new balance: ~w", [N, NewBalance]),
    {reply, Reply, NewState};

handle_call({withdraw, N}, _From, State=#state{ balance = Balance })->
    lager:error("Unable to withdraw ~w, the balance is: ~w", [N, Balance]),
    {reply, {error, "Insufficient funds!"}, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(Reason, State) ->
    lager:info("Terminating server; reason: ~w, state: ~w", [Reason, State]),
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%% Internal functions
